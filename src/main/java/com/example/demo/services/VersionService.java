package com.example.demo.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class VersionService {

  ApplicationContext applicationContext;

  @Autowired
  public VersionService(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  public String getVersion() {
      String version = applicationContext.getBeansWithAnnotation(SpringBootApplication.class).entrySet().stream()
          .findFirst()
          .flatMap(es -> {
            final String implementationVersion = es.getValue().getClass().getPackage().getImplementationVersion();
            return Optional.ofNullable(implementationVersion);
          }).orElse("unknown");

    return version;
  }

}
